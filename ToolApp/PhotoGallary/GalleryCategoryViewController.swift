//
//  GalleryCategoryViewController.swift
//  ToolApp
//
//  Created by Phaninder on 05/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class GalleryCategoryViewController: BaseClassVC {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!

    var showMenuButton = true
    private let refreshControl = UIRefreshControl()
    var eventId: String?
    var eventData: EventData!
    var categories: [GalleryCategory]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id

        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        self.titleLabel.text = "Albums".localized

        
        self.collectionView.dataSource = self
        self.collectionView.delegate   = self
        self.collectionView.isScrollEnabled = true
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.scrollDirection = .vertical
        }
        
        self.collectionView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    @objc func refreshData(_ sender: Any) {

        //        fetchGallery()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GalleryCategoryViewController: UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.cellIdentifier(), for: indexPath) as! CategoryCollectionViewCell
        cell.categoryName.text = categories[indexPath.row].name ?? ""
        cell.categoryImageView.sd_setImage(with: URL(string: categories[indexPath.row].pictures[0]), placeholderImage: UIImage(named: "gallery_placeholder"), options: .retryFailed, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "PhotoGallaryVc") as! PhotoGallaryVc
        destination.galleryCategory = categories[indexPath.row]
        destination.eventData = eventData
        destination.showMenuButton = false
        self.navigationController?.pushViewController(destination, animated: true)
    }
    

}

extension GalleryCategoryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.bounds.width / 2) - 15 , height: 175.0)
        
    }
}

