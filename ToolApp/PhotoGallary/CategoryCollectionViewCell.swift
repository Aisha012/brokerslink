//
//  CategoryCollectionViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 05/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    
    class func cellIdentifier() -> String {
        return "CategoryCollectionViewCell"
    }
    
    
}
