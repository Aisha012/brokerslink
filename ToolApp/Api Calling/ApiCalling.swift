//
//  ApiCalling.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation

import Foundation
import Alamofire

typealias NetworkClosure = (_ netResponse:ASNetObject) -> (Void)

struct CallApi{
    //"Bearer 113cf28b-728f-4696-8aa7-8f4365f6c475"
    static func getData( url:String ,parameter:Dictionary<String, Any>? , type:HTTPMethod , callback:@escaping NetworkClosure) -> DataRequest {
        let headers: HTTPHeaders = ["Content-Type": "Application/json"]
        let request = Alamofire.request(url, method: type, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.request as Any)  // original URL request
            print(response.response as Any) // URL response
            print(response.data as Any)     // server data
            print(response.result)   // result of response serialization
            print(response.description)
            var operationSuccessful: Bool?
            var message: String?
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    message =   data as? String
                    operationSuccessful =   true
                }
                break
                
            case .failure(_):
                operationSuccessful =   false
                break
            }
            
            let responseObj = ASNetObject.init(response: response.result.value as AnyObject?, err: response.result.error as NSError?, status: response.response?.statusCode, success:response.result.isSuccess, opSuccess: operationSuccessful, message: message)
            callback(responseObj);
        }
        return request
    }
    
    static func uploadArrayList( url:String ,parameter:Dictionary<String,Any>? , type:HTTPMethod , callback:@escaping NetworkClosure)  {
        let headers: HTTPHeaders = ["Content-Type": "Application/json", "Authorization": "Bearer 681ddd7e-0db4-4ebb-800c-3259f16758b0"]
        
        var request = URLRequest(url: NSURL(string:url)! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields =   headers
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameter!)
        
        Alamofire.request(request).responseJSON { (response) in
            
//            print(response.request as Any)  // original URL request
//            print(response.response as Any) // URL response
//            print(response.data as Any)     // server data
//            print(response.result)   // result of response serialization
//            print(response.description)
            var operationSuccessful: Bool?
            var message: String?
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
//                    print(response.result.value as Any)
                    message =   data as? String
                    operationSuccessful =   true
                }
                break
            case .failure(_):
//                print(response.result.error as Any)
                operationSuccessful =   false
                break
                
            }
            let responseObj = ASNetObject.init(response: response.result.value as AnyObject?, err: response.result.error as NSError?, status: response.response?.statusCode, success:response.result.isSuccess, opSuccess: operationSuccessful, message: message)
            callback(responseObj);
        }
        
    }
    
}
