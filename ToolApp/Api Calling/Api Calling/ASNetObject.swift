//
//  ASNetObject.swift
//  ToolApp
//
//  Created by Zaman Meraj on 26/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation


struct ASNetObject {
    
    let responseDict:AnyObject?
    let error:NSError?
    let statusCode:Int?
    let isSuccess:Bool?
    let operationSuccessful:Bool?
    let serverMessage : String?
    
    
    init(response:AnyObject?, err:NSError? , status:Int?, success:Bool, opSuccess:Bool?, message:String?){
        
        self.statusCode = status;
        self.isSuccess  = success;
        self.error = err;
        self.responseDict = response;
        self.operationSuccessful = opSuccess;
        self.serverMessage = message;
    }
}
