//
//  EventOption.swift
//  ToolApp
//
//  Created by Phaninder on 16/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class EventOption {
    
    let id: String!
    let apiToken: String!
    let name: String!
    let logo: String!
    
    init(responseDict: NSDictionary) {
        self.id = String(responseDict["id"] as? Int ?? 0)
        self.apiToken = responseDict["api_token"] as? String ?? ""
        self.name = responseDict["name"] as? String ?? ""
        self.logo = responseDict["logo"] as? String ?? ""
    }
    
    
}
