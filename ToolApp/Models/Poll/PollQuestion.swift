//
//  PollQuestion.swift
//  ToolApp
//
//  Created by Phaninder on 17/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class PollQuestion {
    
    let id: String!
    let question: String!
    
    init(responseDict: NSDictionary) {
    
        self.id = String(responseDict.value(forKey: "id") as? Int ?? 0)
        self.question = responseDict.value(forKey: "text") as? String ?? ""

    }
    
}
