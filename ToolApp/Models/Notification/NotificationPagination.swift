//
//  NotificationPagination.swift
//  ToolApp
//
//  Created by Phaninder on 30/04/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class NotificationPagination {
    
    var notifications: [NotificationModal] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool = false
    
    init() {
        notifications = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    init(array: [NSDictionary]) {
        for dict in array {
            let notification = NotificationModal(responseDict: dict)
            self.notifications.append(notification)
        }
        
        hasMoreToLoad = self.notifications.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: NotificationPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.notifications = [];
            self.notifications = newPaginationObject.notifications
        case .old:
            self.notifications += newPaginationObject.notifications
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
