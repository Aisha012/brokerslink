//
//  Notification.swift
//  ToolApp
//
//  Created by Phaninder on 30/04/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import Foundation

class NotificationModal {
    
    let id: Int!
    let title: String!
    let message: String!
    let user_id: String!
    let notification_type: String!
    let redirect_id: Int!
    let created_at: String!
    let createdDate: Date!
    let elapsedTime: String!
    

    init(responseDict: NSDictionary) {
        self.id = responseDict.value(forKey: "id") as? Int ?? 0
        self.title = responseDict.value(forKey: "title") as? String ?? ""
        self.message = responseDict.value(forKey: "message") as? String ?? ""
        self.user_id = responseDict.value(forKey: "user_id") as? String ?? ""
        self.notification_type = responseDict.value(forKey: "notification_type") as? String ?? ""
        self.redirect_id = responseDict.value(forKey: "redirect_id") as? Int ?? 0
        self.created_at = responseDict.value(forKey: "created_at") as? String ?? ""
        if let date = self.created_at, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 16)
            let myDateString = String(date[..<index])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            createdDate = dateFormatter.date(from: myDateString)!
            elapsedTime = createdDate.getElapsedInterval()
        } else {
            createdDate = Date()
            elapsedTime = createdDate.getElapsedInterval()
        }

    }
}


