
//
//  DocumentTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var holderView: UIView!

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }

    class func cellIdentifier() -> String {
        return "DocumentTableViewCell"
    }
    
    func configureCell(doc: Document) {
        captionLabel.text = doc.caption
        var imageName = ""
        switch doc.fileType {
        case "doc/msword":
            imageName = "msword"
        case "doc/pdf":
            imageName = "pdf"
        case "doc/powerpoint":
            imageName = "ppt"
        default:
            imageName = "image"
        }
        docImageView.image = UIImage(named: imageName)
    }
    
}
