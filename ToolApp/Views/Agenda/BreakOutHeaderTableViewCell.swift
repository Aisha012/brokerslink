//
//  BreakOutHeaderTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 26/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class BreakOutHeaderTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "BreakOutHeaderTableViewCell"
    }
    
}
