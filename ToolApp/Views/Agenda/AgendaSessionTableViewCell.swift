//
//  AgendaSessionTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 22/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaSessionTableViewCell: UITableViewCell {

    @IBOutlet weak var sessionTypeLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "AgendaSessionTableViewCell"
    }

}
