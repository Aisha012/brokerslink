//
//  AgendaNotesTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 06/05/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

protocol AgendaNotesTableViewCellDelegate {
    func submitNotes(_ notes: String)
}

class AgendaNotesTableViewCell: UITableViewCell {

    @IBOutlet weak var notesTextView: UITextView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var delegate: AgendaNotesTableViewCellDelegate?
    
    class func cellIdentifier() ->  String {
        return "AgendaNotesTableViewCell"
    }
   
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        delegate?.submitNotes(notesTextView.text)
    }
}
