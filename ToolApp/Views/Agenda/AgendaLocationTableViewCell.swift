//
//  AgendaLocationTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 22/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var locationNameLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "AgendaLocationTableViewCell"
    }

}
