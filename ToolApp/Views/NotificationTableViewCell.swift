//
//  NotificationTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 30/04/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit


class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "NotificationTableViewCell"
    }
    
    func configureCellWithNotification(_ notification: NotificationModal) {
        cellImageView.image = UIImage(named: "tile_chat")
        titleLabel.text = notification.title
        messageLabel.text = notification.message
        timeLabel.text = notification.elapsedTime
    }
    
}
