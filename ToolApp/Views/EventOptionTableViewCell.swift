//
//  EventOptionTableViewCell.swift
//  ToolApp
//
//  Created by Phaninder on 16/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class EventOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    class func cellIdentifier() -> String {
        return "EventOptionTableViewCell"
    }
    
}
