//
//  QuestionListCell.swift
//  CCBT Unified
//
//  Created by devlopment on 07/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

protocol QuestionListCellDelegate {
    func likeButtonTapped(_ index: Int)
}

class QuestionListCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    
    @IBOutlet weak var voteNumberLabel: UILabel!
    @IBOutlet weak var voteTextLabel: UILabel!
    @IBOutlet weak var thumbsUpButton: UIButton!

    var delegate: QuestionListCellDelegate?

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
        
    }
    
    func configureCell(question: Question, atIndex index: Int) {
        thumbsUpButton.tag = index
        let avatarURL = question.avatar ?? ""
        profilePic.sd_setShowActivityIndicatorView(true)
        profilePic.sd_setIndicatorStyle(.gray)
        profilePic.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "profile.png"), options: .progressiveDownload, completed: nil)
        
        nameLabel.text = question.firstName + " " + question.lastName
        dateLabel.text = question.formattedDate
        questionLabel.text = question.title
        let likeImage = UIImage(named: question.hasVoted ? "like" : "unlike")
        thumbsUpButton.setImage(likeImage, for: .normal)
        voteNumberLabel.text = String(question.votes)
        voteTextLabel.text = question.votes == 1 ? "vote" : "votes"

        
    }

    @IBAction func thumbsUpButtonTapped(_ sender: UIButton) {
        delegate?.likeButtonTapped(sender.tag)
    }
}
