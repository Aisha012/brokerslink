//
//  AgendaCell.swift
//  ToolApp
//
//  Created by mayank on 28/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit

class AgendaCell: UITableViewCell {
    @IBOutlet weak var mrngTime: UILabel!
    @IBOutlet weak var eveTime: UILabel!
    @IBOutlet weak var agendaName: UILabel!
    @IBOutlet weak var agenda_des: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
