//
//  AddressVC.swift
//  ToolApp
//
//  Created by mayank on 30/11/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddressVC: BaseClassVC, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var bottomView: UIView!    
    @IBOutlet weak var address_out: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet var mapView: MKMapView!

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var eventId: String?
    var eventData: EventData!
    var showMenuButton = true
    var eventLocation: EventLocation!
    let regionRadius: CLLocationDistance = 1000

    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventId = eventData.event_id
        if showMenuButton {
            let revealViewController = self.revealViewController()
            menuButton.addTarget(revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        } else {
            menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
            menuButton.setImage(nil, for: .normal)
            backImage.image = UIImage(named: "back.png")
        }
        self.titleLabel.text = "Venue"//"Access".localized
        mapView!.showsPointsOfInterest = true
        if let mapView = self.mapView {
            mapView.delegate = self
        }
        if #available(iOS 11.0, *) {
            mapView.register(EventMapView.self,
                             forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        } else {
            // Fallback on earlier versions
        }

        self.AddressApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        bottomView.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func shareAddressButtonTapped(_ sender: UIButton) {
        
        var shareArray = [Any]()
        if UIApplication.shared.canOpenURL(URL.init(string: "comgooglemaps://")!) {
            shareArray.append(URL.init(string: "comgooglemapsurl://maps.google.com/?q=\(self.eventLocation.coordinate.latitude),\(self.eventLocation.coordinate.longitude)")!)
        } else {
            shareArray.append(URL.init(string: "https://maps.google.com/?q=\(self.eventLocation.coordinate.latitude),\(self.eventLocation.coordinate.longitude)")!)
        }
        let activityController = UIActivityViewController(activityItems: shareArray, applicationActivities: nil)

        self.present(activityController, animated: true, completion: nil)
    }
    
    
    func setUpTimerLabel() {
        let timer = Timer.scheduledTimer(timeInterval: 1,
                                         target: self,
                                         selector: #selector(self.updateTime),
                                         userInfo: nil,
                                         repeats: true)
        RunLoop.current.add(timer, forMode: .commonModes)
    }
    
    @objc func updateTime() {
        self.titleLabel.text = getElapsedTimefromDate(startTime: eventData?.start_time, endTime: eventData?.end_time)
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        let coordinate = CLLocationCoordinate2D(latitude: 52.520007, longitude: 13.404954)
//        shareLocation(coordinate: coordinate)
//        let vCardURL = LocationVCard.vCardUrl(from: coordinate, with: "Berlin")
//        let activityViewController = UIActivityViewController(activityItems: [vCardURL], applicationActivities: nil)
//        present(activityViewController, animated: true, completion: nil)

    }
    
    func shareLocation(coordinate:CLLocationCoordinate2D) {
        
        guard let cachesPathString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else {
            print("Error: couldn't find the caches directory.")
            return
        }
        
        let vCardString = [
            "BEGIN:VCARD",
            "VERSION:3.0",
            "N:;Shared Location;;;",
            "FN:Shared Location",
            "item1.URL;type=pref:http://maps.apple.com/?ll=\(coordinate.latitude),\(coordinate.longitude)",
            "item1.X-ABLabel:map url",
            "END:VCARD"
            ].joined(separator: "\n")
        
        let vCardFilePath = (cachesPathString as NSString).appendingPathComponent("vCard.loc.vcf")
        
        do {
            try vCardString.write(toFile: vCardFilePath, atomically: true, encoding: String.Encoding.utf8)
            let activityViewController = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: vCardFilePath)], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivityType.print,
                                                            UIActivityType.copyToPasteboard,
                                                            UIActivityType.assignToContact,
                                                            UIActivityType.saveToCameraRoll,
                                                            UIActivityType.postToVimeo,
                                                            UIActivityType.postToTencentWeibo,
                                                            UIActivityType.postToWeibo]
            present(activityViewController, animated: true, completion: nil)
        } catch let error {
            print("Error, \(error), saving vCard: \(vCardString) to file path: \(vCardFilePath).")
        }
    }
    
    func AddressApi() {
        let auth_Token = UserDefaults.standard.value(forKey: "Auth_Token") as? String
        if let auth_Token = auth_Token {
            self.showHUD(forView: self.view, excludeViews: [])
        _ = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(eventId!)/access?user_token=\(auth_Token)&api_token=\(apiToken)", parameter: nil, type: .get) { (netResponse) -> (Void) in
                if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){
                    
                    if let JSON = netResponse.responseDict as? NSDictionary {
                        self.eventLocation = EventLocation.init(json: JSON)
                        self.address_out.text = self.eventLocation.locationName
                        
                        self.mapView.addAnnotations([self.eventLocation])
                        self.mapView.delegate = self
                        self.centerMapOnLocation(location: CLLocation(latitude: self.eventLocation.coordinate.latitude, longitude: self.eventLocation.coordinate.longitude))
                        self.hideHUD(forView: self.view)
                    }else{
                        self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                    }
                    self.hideHUD(forView: self.view)
                }else {
                    self.view.makeToast("Something didn't go as expected".localized, duration: 2.0, position: CSToastPositionCenter)
                    self.hideHUD(forView: self.view)
                }
                self.hideHUD(forView: self.view)
            }
        }
        
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! EventLocation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

}

