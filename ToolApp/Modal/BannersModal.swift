//
//  BannersModal.swift
//  ToolApp
//
//  Created by Zaman Meraj on 03/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation


struct BannersModal {
    
    var bannerId: String?
    var bannerTitle: String?
    var bannerDescription: String?
    var bannerImage: String?
    var eventId: String?
    var bannerVideo: String?
    
    init(responseDict: NSDictionary, eventId: String) {
        
        self.bannerId          = "\(responseDict.value(forKey: "id") as! Int)"
        self.bannerTitle       = responseDict.value(forKey: "title") as? String
        self.bannerDescription = responseDict.value(forKey: "description") as? String
        self.bannerImage       = responseDict.value(forKey: "banner") as? String
        self.bannerVideo = responseDict.value(forKey: "video") as? String
        self.eventId           = eventId
        
    }
    
    
}
