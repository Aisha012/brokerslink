//
//  COmpleteAgendaModal.swift
//  ToolApp
//
//  Created by Zaman Meraj on 07/12/17.
//  Copyright © 2017 Zaman Meraj. All rights reserved.
//

import Foundation

struct CompleteAgendaModal {
    
    var date: String?
    var list = [AgendaListModal]()
    
    init(responseDic: NSDictionary, eventId: String) {
        self.date = responseDic.value(forKey: "date") as? String
        
        if let lists = responseDic.value(forKey: "list") as? [NSDictionary] {
            for list in lists {
                let listModal = AgendaListModal(responseDict: list, eventId: eventId)
                self.list.append(listModal)
            }
        }
    }
}
