//
//  SideManu_Vc.swift
//  sideMenu
//
//  Created by Prashant on 20/12/17.
//  Copyright © 2017 Prashant. All rights reserved.
//

import UIKit

class SideManu_Vc: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib.init(nibName: "SideManu_TVC", bundle: nil), forCellReuseIdentifier: "SideManu_TVC")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SideManu_Vc : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell    =   tableView.dequeueReusableCell(withIdentifier: "SideManu_TVC", for: indexPath) as! SideManu_TVC
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("DidSelect Clicked")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.09
    }
}
