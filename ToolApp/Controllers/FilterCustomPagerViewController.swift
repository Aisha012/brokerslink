//
//  FilterCustomPagerViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class FilterCustomPagerViewController: ButtonBarPagerTabStripViewController {

    var completeModal = [CompleteAgendaModal]()
    var eventId: String!

    override func viewDidLoad() {
        self.settings.style.selectedBarHeight = 3
        self.settings.style.buttonBarMinimumInteritemSpacing = 10
        let buttonColor: UIColor!
        if let color_Value  =  UserDefaults.standard.value(forKey: "color_Value") as? String {
            buttonColor = UIColor.colorWithHexString(hex: color_Value)
        }else{
            buttonColor = UIColor.init(red: 0.0/255.0, green: 195.0/255.0, blue: 231/255.0, alpha: 1)
        }
        
        self.settings.style.selectedBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = buttonColor
        self.settings.style.buttonBarItemTitleColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = buttonColor
        self.settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 17.0)
        super.viewDidLoad()

    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let categoryVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        categoryVC.eventId = self.eventId
        categoryVC.completeModal = self.completeModal
        
        let roomVC = self.storyboard?.instantiateViewController(withIdentifier: "RoomViewController") as! RoomViewController
        roomVC.eventId = self.eventId
        roomVC.completeModal = self.completeModal
        
        let dateVC = self.storyboard?.instantiateViewController(withIdentifier: "DateViewController") as! DateViewController
        dateVC.eventId = self.eventId
        dateVC.completeModal = self.completeModal
        
        return [categoryVC, roomVC, dateVC]
    }

}
