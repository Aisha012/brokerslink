//
//  ChatListViewController..swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Starscream
import ActionCableClient
import Alamofire

class ChatListViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    var eventId: String?
    var eventData: EventData!
    var chatPagination = ConversationListPagination()
    private let refreshControl = UIRefreshControl()
    var isFirstTime = true
    var isFetchingData = false
    var userData: LoginData!
    var client: ActionCableClient!
    var searchString = ""
    var currentIndex = "1"
    var request: DataRequest!

    override func viewDidLoad() {
        super.viewDidLoad()
        eventId = eventData.event_id
        let loginData = MyDatabase.getProgramData(tableName: "LoginData") as? [LoginData]
        userData = loginData![0]

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(searchFieldChanged(_:)),
                                               name: .fetchSearchData,
                                               object: nil)

        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.isFirstTime = true
        fetchChatList()
    }

    deinit {

    }
    
    @objc func searchFieldChanged(_ notification: NSNotification) {
        if let search = notification.userInfo?["data"] as? String,
        let page = notification.userInfo?["page"] as? String {
            self.searchString = search
            if page == currentIndex {
                if let req = request {
                    req.cancel()
                }
                self.chatPagination.paginationType = .reload
                self.chatPagination.currentPageNumber = 1
                self.fetchChatList()
            }
        }
    }

    @objc func refreshData(_ sender: Any) {
        self.chatPagination.paginationType = .reload
        fetchChatList()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Conversations".localized)
    }

    func showViewsAccordingly() {
        if chatPagination.conversations.count == 0 {
            tableView.isHidden = true
            emptyPlaceholderLabel.isHidden = false
        } else {
            tableView.isHidden = false
            emptyPlaceholderLabel.isHidden = true
        }
        tableView.reloadData()

    }
    
    func fetchChatList(){
        let user_token = userData.authenticate_user
        if chatPagination.paginationType != .old {
            self.chatPagination.currentPageNumber = 1
            if isFirstTime == true {
                self.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        isFetchingData = true

        request = CallApi.getData(url: "http://eu.aio-events.com/api/events/\(self.eventId!)/conversations?user_token=\(user_token!)&api_token=\(apiToken)&page=\(chatPagination.currentPageNumber)&query=\(searchString)", parameter: nil, type: .get) { (netResponse) -> (Void) in
            self.isFetchingData = false
            if((netResponse.isSuccess == true) && (netResponse.operationSuccessful != nil) && (netResponse.operationSuccessful != false) && (netResponse.statusCode == 200)){

                if let JSON = netResponse.responseDict as? NSDictionary, let conversationsArray = JSON["conversations"] as? [NSDictionary] {
                    let newPagination = ConversationListPagination(array: conversationsArray)
                    self.chatPagination.appendDataFromObject(newPagination)
                    DispatchQueue.main.async {
                        if self.chatPagination.paginationType != .old && self.isFirstTime == true {
                            self.hideHUD(forView: self.view)
                            self.isFirstTime = false
                        } else {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        self.showViewsAccordingly()
                    }
                } else {
                    self.showViewsAccordingly()
                    self.hideHUD(forView: self.view)
                }
            }else {
                self.hideHUD(forView: self.view)
            }
        }
    }

}

extension ChatListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return chatPagination.conversations.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < chatPagination.conversations.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: ConversationTableViewCell.cellIdentifier(), for: indexPath) as! ConversationTableViewCell
        cell.configureCell(convo: chatPagination.conversations[indexPath.row ])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < chatPagination.conversations.count else {
            return chatPagination.hasMoreToLoad ? 50.0 : 0.0
        }
        return 86.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.chatPagination.conversations.count && self.chatPagination.hasMoreToLoad && !isFetchingData {
            fetchChatList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        destination.eventData = self.eventData
        destination.userData = self.userData
        destination.chatUser = ChatUser(conversation: chatPagination.conversations[indexPath.row])
        destination.conversationId = chatPagination.conversations[indexPath.row].id
        destination.participantName = chatPagination.conversations[indexPath.row].firstName
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.pushViewController(destination, animated: true)
        
    }


}
