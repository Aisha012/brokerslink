//
//  PollPageViewController.swift
//  ToolApp
//
//  Created by Phaninder on 18/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class PollPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var polls = [Poll]()
    var viewControllerArray = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        for (index, poll) in polls.enumerated() {
            let pollVC = self.storyboard?.instantiateViewController(withIdentifier: "PollChildViewController") as! PollChildViewController
            pollVC.poll = poll
            pollVC.pageIndex = index
            viewControllerArray.append(pollVC)
        }
        
        if let firstVC = viewControllerArray.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }

    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = viewControllerArray.index(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return viewControllerArray.last }
        
        guard viewControllerArray.count > previousIndex else { return nil        }
        
        return viewControllerArray[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = viewControllerArray.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < viewControllerArray.count else { return viewControllerArray.first }
        
        guard viewControllerArray.count > nextIndex else { return nil         }
        
        return viewControllerArray[nextIndex]
    }
    

}
