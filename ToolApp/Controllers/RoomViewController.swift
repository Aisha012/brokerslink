//
//  RoomViewController.swift
//  ToolApp
//
//  Created by Phaninder on 06/03/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class RoomViewController: BaseClassVC, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    
    var completeModal = [CompleteAgendaModal]()
    var eventId: String!
    var rooms = [RoomModal]()
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        for agendas in completeModal {
            for agenda in agendas.list {
                if let room = agenda.roomModal {
                    let containsAlready = rooms.contains(where: { (roomModal) -> Bool in
                        return room.name == roomModal.name
                    })
                    if !containsAlready {
                        rooms.append(room)
                    }
                }
            }

        }
        emptyPlaceholderLabel.isHidden = !rooms.isEmpty
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Location".localized)
    }

}

extension RoomViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckableTableViewCell",
                                                 for: indexPath) as! CheckableTableViewCell
        cell.textLabel?.text = rooms[indexPath.row].name ?? "No Text".localized
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell =  tableView.cellForRow(at: indexPath) as! CheckableTableViewCell
        var room = ""
        if cell.isSelected {
            tableView.deselectRow(at: indexPath, animated: true)
            let userInfo = ["data": room];
            NotificationCenter.default.post(name: .roomSelected, object: nil, userInfo:userInfo)
            
            return nil
        } else {
            room = rooms[indexPath.row].id!
            let userInfo = ["data": room];
            NotificationCenter.default.post(name: .roomSelected, object: nil, userInfo:userInfo)
            return indexPath
        }
        
    }

    
}
