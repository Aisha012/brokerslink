//
//  ChatParentViewController.swift
//  ToolApp
//
//  Created by Phaninder on 17/02/18.
//  Copyright © 2018 Zaman Meraj. All rights reserved.
//

import UIKit

class ChatParentViewController: BaseClassVC {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var customNavBar: UIView!

    var eventData: EventData!
    var eventId: String!
    var conversationId: String!
    var friendId: String!
    var userData: LoginData!

    override func viewDidLoad() {
        super.viewDidLoad()
        menuButton.addTarget(self, action: #selector(navigateBack), for: .touchUpInside)
        menuButton.setImage(nil, for: .normal)
        backImage.image = UIImage(named: "back.png")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customNavBar.backgroundColor = navigationBarColor()
        self.navigationController?.navigationBar.isHidden = true
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatSeque" {
            let chatViewController = segue.destination as! ChatViewController
            chatViewController.eventData = self.eventData
            chatViewController.userData = self.userData
            chatViewController.conversationId = conversationId
//            chatViewController.preferredContentSize = CGSize(width: 200, height: 300)
        }
    }

}
